import React from 'react';
import { StyleSheet, View, Button, Text } from 'react-native';
import List from './src/components/List'
import SendTransaction from './src/components/SendTransaction'
import Sum from './src/components/Sum'

export default class App extends React.Component {
  state = {
    isModalOpen: false,
    categories: [],
    isLoading: true,
    justAdded: false,
  }
  componentDidMount() {
    fetch('https://tranf-ae713.firebaseio.com/cat.json')
      .then(resp => resp.json())
      .then(parsed => {
        const categories = []
        Object.keys(parsed).map(key => categories.push(parsed[key]))
        this.setState({ categories, isLoading: false })
      })
  }
  toggleModal = bool => this.setState({ isModalOpen: bool })
  setJustAdded = () => {
    this.setState({ justAdded: true })
    setTimeout(() => this.setState({ justAdded: false }), 1000)
  }
  render() {
    return !this.state.isLoading ? (
      <View style={styles.container}>
        <SendTransaction
          isModalOpen={this.state.isModalOpen}
          closeModal={() => this.toggleModal(false)}
          categories={this.state.categories}
          setJustAdded={this.setJustAdded}
        />
        <View style={styles.btn}><Button title="Add" onPress={() => this.toggleModal(true)}/></View>
        <Sum justAdded={this.state.justAdded} />
        <List categories={this.state.categories} justAdded={this.state.justAdded} />
      </View>
    ) : <Text>Loading...</Text>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    width: '90%',
    margin: 15,
  }
});
