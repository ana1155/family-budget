import React from 'react'
import { Modal, View, TextInput, Button, Text, Picker, StyleSheet } from 'react-native'

class SendTransaction extends React.Component {
  state = {
    amount: '',
    category: this.props.categories[0].id,
    description: '',
  }
  handleAmountChange = val => this.setState({ amount: val })
  handleCategoryChange = val => this.setState({ category: val })
  handleDescriptionChange = val => this.setState({ description: val })
  handleSubmit = () => {
    const year = new Date().getFullYear()
    const month = new Date().getMonth() + 1
    fetch(`https://tranf-ae713.firebaseio.com/transaction/${year}/${month}.json`, {
      method: 'post',
      body: JSON.stringify({
        amount: parseInt(this.state.amount, 10),
        category: this.state.category,
        description: this.state.description,
        date: Date.now()
      })
    })
      .then(this.props.setJustAdded)
      .catch(err => console.log(err))
    this.setState({
      amount: '',
      category: this.props.categories[0].id,
      description: '',
    })
    this.props.closeModal()
  }
  render() {
    return (
      <Modal
        visible={this.props.isModalOpen}
        animationType="slide"
        onRequestClose={this.props.closeModal}
      >
        <View style={styles.container}>
          <View style={styles.btns}>
            <Button title="Продукты" onPress={() => this.handleCategoryChange(31)} />
            <Button title="Хозтовары" onPress={() => this.handleCategoryChange(34)} />
            <Button title="Транспорт" onPress={() => this.handleCategoryChange(11)} />
          </View>
          <View style={styles.btns}>
            <Button title="Такси" onPress={() => this.handleCategoryChange(12)} />
            <Button title="Кафе" onPress={() => this.handleCategoryChange(52)} />
            <Button title="Кабаки" onPress={() => this.handleCategoryChange(22)} />
            <Button title="Обед" onPress={() => this.handleCategoryChange(14)} />
          </View>
          <View style={styles.inputBlock}>
            <Text>Amount</Text>
            <TextInput style={styles.input} keyboardType="numeric" onChangeText={this.handleAmountChange}/>
          </View>
          <View style={styles.inputBlock}>
            <Text>Category</Text>
            <Picker
              selectedValue={this.state.category}
              style={styles.select}
              onValueChange={this.handleCategoryChange}>
              {this.props.categories.map(cat => (
                <Picker.Item key={cat.id} label={cat.name.toUpperCase()} value={cat.id} />
              ))}
            </Picker>
          </View>
          <View style={styles.inputBlock}>
            <Text>Description</Text>
            <TextInput style={styles.input} onChangeText={this.handleDescriptionChange}/>
          </View>
          <View style={styles.btn}>
            <Button title="save" onPress={this.handleSubmit} disabled={!this.state.amount}/>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btns: {
    width: '90%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  shortcut: {
    width: '24%',
  },
  inputBlock: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: '100%',
    flexShrink: 1,
    margin: 10,
  },
  select: {
    height: 50,
    width: '100%',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'grey',
    borderRadius: 3,
    flexShrink: 1,
  },
  btn: {
    width: '90%',
    margin: 15,
  }
});

export default SendTransaction
