import React from 'react'
import { StyleSheet, View, Text, ScrollView, RefreshControl } from 'react-native';

class List extends React.Component {
  state = {
    isLoading: true,
    listItems: [],
    refreshing: false,
  }
  fetchData = () => {
    const year = new Date().getFullYear()
    const month = new Date().getMonth() + 1
    fetch(`https://tranf-ae713.firebaseio.com/transaction/${year}/${month}.json`)
      .then(resp => resp.json())
      .then(parsed => {
        const items = []
        Object.keys(parsed).map(key => items.push(parsed[key]))
        this.setState({ listItems: items.reverse(), isLoading: false, refreshing: false })
      })
      .catch(err => {
        console.log(err)
        this.setState({ isLoading: false, refreshing: false })
      })
  }
  componentDidMount() {
    this.fetchData()
  }
  componentDidUpdate(oldProps) {
    if (!oldProps.justAdded && this.props.justAdded) {
      this.setState({ refreshing: true })
      this.fetchData()
    }
  }
  onRefresh = () => {
    this.setState({ refreshing: true })
    this.fetchData()
  }
  render() {
    const { isLoading, listItems } = this.state
    return !isLoading ? (
      <ScrollView style={styles.container} refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
        />
      }>
        {listItems.length > 0 ? listItems.map(transaction => {
          return (
            <View key={transaction.date} style={styles.listItem}>
              <View style={styles.itemPart}>
                <View style={styles.amount}>
                  <Text style={styles.sum}>{transaction.amount}</Text>
                </View>
                <Text style={styles.category}>
                  {this.props.categories.find(cat => cat.id === transaction.category).name}
                  </Text>
              </View>
              <View style={styles.itemPart}>
                <Text>{transaction.description}</Text>
                <Text>{new Date(transaction.date).toDateString()}</Text>
              </View>
            </View>
          )
        }) : <Text>No transactions founds, yet.</Text>}
      </ScrollView>
    ) : <Text>Loading...</Text>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '90%',
  },
  listItem: {
    width: '100%',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'grey',
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    display: 'flex',
    padding: 5,
  },
  itemPart: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  amount: {
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: 'red',
    borderRadius: 3,
    minWidth: 45,
    padding: 3,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  sum: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  category: {
    fontWeight: 'bold',
    fontSize: 18,
  },
});

export default List
