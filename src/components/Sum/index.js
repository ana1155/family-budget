import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

class Sum extends React.Component {
  state = {
    isLoading: true,
    sum: null,
  }
  monthFormatter = () => {
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ]
    const index = new Date().getMonth()
    return months[index]
  }
  fetchData = () => {
    fetch('https://us-central1-tranf-ae713.cloudfunctions.net/r')
      .then(resp => resp.json())
      .then(parsed => {
        this.setState({ isLoading: false, sum: parsed.total })
      })
      .catch(err => console.log(err))
  }
  componentDidMount() {
    this.fetchData()
  }
  componentDidUpdate(oldProps) {
    console.log(oldProps.justAdded, this.props.justAdded)
    if (!oldProps.justAdded && this.props.justAdded) {
      this.setState({ refreshing: true })
      this.fetchData()
    }
  }
  render() {
    return (
      <View style={styles.amount}>
        <Text style={styles.date}>
          {this.monthFormatter()} {new Date().getFullYear()}
        </Text>
        {!this.state.isLoading
          ? <Text style={styles.sum}>( {this.state.sum} )</Text>
          : <Text>Loading...</Text>}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  amount: {
    padding: 3,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
  },
  date: {
    fontWeight: 'bold',
    fontSize: 16,
    marginRight: 5,
  },
  sum: {
    fontWeight: 'bold',
    color: 'red',
    fontSize: 16,
  },
});

export default Sum
